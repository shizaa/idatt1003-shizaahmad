# Portfolio project IDATT1003 - 2023

STUDENT NAME = Shiza Ahmad  
STUDENT ID = 111725

## Project description

This project is made for the subject IDATT1003 - Programmering 1. The program represents a Train Dispatch System from a
station in Norway.
The application allows the user to manage the dispatching of train departures from the station Oslo S.
The user can choose between a variety of commands to perform on the station, such as adding or deleting departures.
It is a menu-driven program that runs until the user exits.

## Project structure

The project is organized into the following components:

- src: Here is the two main folders for the program: main and test
- main: Includes multiple packages. The "edu.ntnu.stud" package, which includes the source files.
    - The classes are sorted into packages based on their functionality:
    - "errorhandling": Contains the errorhandling class
    - "launcher": Contains the app class for launching the project.
    - "model": Contains the classes concerning the departures and managing them. This includes TrainDeparture,
      TrainDispatchAnalytics and TrainDispatchRegister.
    - "view": Contains the classes concerning the user interface. This includes TrainDepartureColorsManager and
      TrainDispatchUi.
- test: Includes the "edu.ntnu.stud" package, which includes two packages for the jUnit test files.
    - "modelTests": Contains the testclasses TrainDepartureTest, TrainDispatchAnalyticsTest and TrainDispatchRegister
    - "viewTests": Contains the test class TrainDepartureColorsManagerTest.

## Link to repository

Link to gitlab repository:
https://gitlab.stud.idi.ntnu.no/shizaa/idatt1003-shizaahmad


## How to run the project
The project should be run from Intellij to avoid compilation problems. 

The main class is "TrainDispatchApp". This is the class containing the main method that launches the program.
When the project launches, the user will be prompted with a welcome and asked to initialize the system clock.
The input of the program is to follow the menu to make dispatch decisions. The inputs required from the user are
describe after choosing a command.
The output is displayed by the program. That is information about the users input and updates regarding the changes made
to the station.
The project should not have any compilation problems and is expected to work nicely and provide a user-friendly ui.
However, the use of scanner.nextLine() can be tricky and can sometimes cause a repetition of sout outputs. This might
occur in the code. In addition, when the user enters "y" to the question "Do you want to try again? (y for yes/ n for
no)", an extra line is printed out. This is because of the use of scanner.nextLine() and the sout statement. This is not
a big issue, but it is something that can be improved in the future. The use of scanner.nextLine() is necessary to
avoid a bug where the program asks the user multiples times without waiting for an input. Here a smaller bug was chosen
instead of a bigger one.


## How to run the tests

To run the tests ensure that JUnit is installed and run the tests from the IDE. The tests are located in the test folder
and can be run from there. 




## References

[//]: # (TODO: Include references here, if any. For example, if you have used code from the course book, include a reference to the chapter.
Or if you have used code from a website or other source, include a link to the source.)
[1]	Wikipedia Contributors. (2023, January 29). Responsibility-driven design. Wikipedia; Wikimedia Foundation. https://en.wikipedia.org/wiki/Responsibility-driven_design 

[2]Norozi, Muhammed A. (2023) Kap 7 Tabeller av primitive datatyper. Hentet fra BlackBoard

[3]	Norozi, Muhammed A. (2023 )Kap 4 Klasser som byggeklosser. Hentet fra BlackBoard

[4]	Horstmann, Cay S. (2022). Core JAVA. Volume 1: Fundamentals. Pearson Education Inc.

[5]	Software Engineering Coupling and Cohesion. (2018, July 30). GeeksforGeeks; GeeksforGeeks. https://www.geeksforgeeks.org/software-engineering-coupling-and-cohesion/

[6]	What Is Usability. (2023, December). GeeksforGeeks; GeeksforGeeks. https://www.geeksforgeeks.org/what-is-usability/

