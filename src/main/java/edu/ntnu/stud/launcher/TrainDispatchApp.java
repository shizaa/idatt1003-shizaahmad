package edu.ntnu.stud.launcher;

import edu.ntnu.stud.view.TrainDispatchUi;

/**
 * This is the main class for the Train Dispatch System. The class contains the main method, which
 * creates a new TrainDispatchUi object, and calls the init and start methods.
 *
 * <p>Responsibility: To start the Train Dispatch System.
 *
 */
public class TrainDispatchApp {

  /**
   * Main method for the Train Dispatch System. The method creates a new TrainDispatchUi object, and
   * calls the init and start methods.
   *
   * @param args The arguments for the main method.
   */
  public static void main(String[] args) {
    TrainDispatchUi app = new TrainDispatchUi();
    app.init();
    app.start();
  }
}
