package edu.ntnu.stud.errorhandling;

import java.util.Scanner;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

/**
 * This class is used to handle user input and display error messages. The class has two methods for
 * handling user input, one for int and one for String. These are used to ensure that the user can
 * only enter valid input, and is given the option to try again if the input is invalid. The class
 * also has methods for checking if the input is valid for the different attributes in the
 * TrainDeparture class.
 *
 * <p>Responsibility: To ensure robust code with error-handling.
 */
public class TrainDispatchErrorhandling {
  /**
   * This is a method made for a better user interface. The method ensures that the user can only
   * enter a valid input, and is given the option to try again if the input is invalid. This is for
   * int input. The user is sent back to the main menu if the input is n.
   *
   * @param scanner The scanner object used to read input from the user
   * @param prompt The prompt to display to the user
   * @param validationCondition The condition that the input must satisfy
   */
  public static int getValidInputWithRetry(
      Scanner scanner, String prompt, IntPredicate validationCondition) {
    int input;
    while (true) {
      System.out.print(prompt);
      if (scanner.hasNextInt()) {
        input = scanner.nextInt();
        if (validationCondition.test(input)) {
          return input;
        }
      } else {
        System.out.println("Invalid input. Please enter a valid value.");
        scanner.nextLine();
      }

      while (true) {
        System.out.print("Do you want to try again? (y for yes /n for no): ");
        String retry = scanner.nextLine().toLowerCase();
        if (retry.equals("n")) {
          return -1;
        } else if (retry.equals("y")) {
          break;
        } else {
          System.out.println("Invalid input. Please enter y or n.");
        }
      }
    }
  }

  /**
   * This is a method made for a better user interface. The method ensures that the user can only
   * enter a valid input, and is given the option to try again if the input is invalid. This is for
   * String input. The user is sent back to the main menu if the input is n.
   *
   * @param scanner The scanner object used to read input from the user
   * @param prompt The prompt to display to the user
   * @param validationCondition The condition that the input must satisfy
   * @return The valid input
   */
  public static String getValidStringInputWithRetryString(
      Scanner scanner, String prompt, Predicate<String> validationCondition) {
    String input;
    while (true) {
      scanner.nextLine();
      System.out.print(prompt);
      input = scanner.nextLine();
      if (validationCondition.test(input)) {
        return input;
      } else {
        System.out.println("Invalid input. Please enter a valid value.");
      }

      while (true) {
        System.out.print("Do you want to try again? (y for yes /n for no): ");
        String retry = scanner.nextLine().toLowerCase();
        if (retry.equals("n")) {
          return null;
        } else if (retry.equals("y")) {
          break;
        } else {
          System.out.println("Invalid input. Please enter y or n.");
        }
      }
    }
  }

  /** This method is used to check for a valid hour. */
  public static int isValidHour(Scanner scanner) {
    scanner.nextLine();
    return getValidInputWithRetry(
        scanner, "Enter the hour (Must be between 0-23): ", hour -> hour >= 0 && hour <= 23);
  }

  /** This method is used to check for a valid minutes. */
  public static int isValidMinutes(Scanner scanner) {
    scanner.nextLine();
    return getValidInputWithRetry(
        scanner,
        "Enter the minutes (Must be between 0-59): ",
        minutes -> minutes >= 0 && minutes <= 59);
  }

  /**
   * This method is used to check for a valid line. The line can be a combination of letters and
   * numbers, but only 4 characters long. This is chosen because there are no lines in Norway that
   * are longer than 4 characters.
   */
  public static String getValidLine(Scanner scanner) {
    return getValidStringInputWithRetryString(
        scanner,
        "Enter the line for the traindeparture "
            + "(A combination of 1-2 capital letters and 1-3 numbers): ",
        line -> line.matches("[A-Z0-9]{1,4}"));
  }

  /**
   * This method is used to check for a valid train number. The train number can only be between
   * 1-9999. This is chosen because seems to be a reasonable number given that the train number is
   * changed every day.
   */
  public static int getValidTrainNumber(Scanner scanner) {
    return getValidInputWithRetry(
        scanner,
        "Enter the train number for the traindeparture (1-9999): ",
        trainNumber -> trainNumber >= 1 && trainNumber <= 9999);
  }

  /**
   * This method is used to check for a valid destination. The destination can only be 40 characters
   * long because that is the maximum length of a station name in Norway from Oslo s. æøåÆØÅ has
   * been added to the regex to allow for Norwegian characters. This is because a compiler such as
   * Intellij supports UTF-8 and can display it. Another compiler might not, and then the regex
   * would not work. However, since this station is in Norway, the best user experience would be to
   * allow for Norwegian characters.
   */
  public static String getValidDestination(Scanner scanner) {
    return getValidStringInputWithRetryString(
        scanner,
        "Enter the destination for the traindeparture (Can only be 40 characters long): ",
        destination -> destination.matches("^[A-ZÆØÅa-zæøå -]{1,41}+$"));
  }

  /**
   * This method is used to check for a valid track. The track can only be between 1-20. because
   * there are a maximum of 20 tracks at Oslo s.
   */
  public static int getValidTrack(Scanner scanner) {
    return getValidInputWithRetry(
        scanner,
        "Enter the track for the traindeparture (Must be between 0-20): ",
        track -> track >= 1 && track <= 20);
  }

  /**
   * This method is used to check for a valid delay. The delay should not be more minutes than there
   * are in an hour.
   */
  public static int getValidDelay(Scanner scanner) {
    return getValidInputWithRetry(
        scanner,
        "Enter the delay for the traindeparture (Must be between 0-59): ",
        delay -> delay >= 0 && delay <= 59);
  }
}
