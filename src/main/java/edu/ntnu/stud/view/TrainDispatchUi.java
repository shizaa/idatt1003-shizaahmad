package edu.ntnu.stud.view;

import static edu.ntnu.stud.errorhandling.TrainDispatchErrorhandling.getValidStringInputWithRetryString;

import edu.ntnu.stud.errorhandling.TrainDispatchErrorhandling;
import edu.ntnu.stud.model.TrainDispatchAnalytics;
import edu.ntnu.stud.model.TrainDispatchRegister;
import java.time.LocalTime;
import java.util.Scanner;

/**
 * This class is used to display the menu and handle user input. The class has a method for
 * initializing the clock and adding some departures to the station. The methods in this class are
 * static because they do not rely on any instance-specific data. The use of GitHub CoPilot has only
 * been used to streamline repetitive code with logic written by the author. Responsibility: To
 * display the menu and handle user input.
 */
public class TrainDispatchUi {

  /** This is the scanner object used to read user input. */
  private final Scanner scanner = new Scanner(System.in);

  /** This is the station object used to store the train departures. */
  private TrainDispatchRegister station;

  /**
   * This method is used to add a new departure to the station. It calls the method
   * retrieveAttributesDeparture from this class to retrieve the attributes for the new departure.
   * It returns if any of the input is invalid. It then adds the departure to the station with
   * either the track constructor or no track constructor and prints the result message by calling
   * the method printAdditionResult.
   *
   * @param scanner The scanner object used to read user input.
   * @param station The station object used to store the train departures.
   */
  private static void addNewDeparture(Scanner scanner, TrainDispatchRegister station) {
    try {
      Result result = retrieveAttributesDeparture(scanner, station);
      if (result == null) {
        return;
      }
      boolean added;

      if (result.track() != -1) {
        added =
            station.addDeparture(
                result.departureHour(),
                result.departureMinutes(),
                result.line(),
                result.trainNumber(),
                result.destination(),
                result.track(),
                result.delay());

      } else {
        added =
            station.addDepartureNoTrack(
                result.departureHour(),
                result.departureMinutes(),
                result.line(),
                result.trainNumber(),
                result.destination(),
                result.delay());
      }

      printAdditionResult(added);

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This method is used to retrieve the attributes for the new departure. It calls the appropriate
   * methods from TrainDispatchErrorhandling to validate the user input. It returns null if any of
   * the input is invalid. It then returns a Result object with the attributes for the new
   * departure. The Result object is used in the method addNewDeparture.
   *
   * @param scanner The scanner object used to read user input.
   * @param station The station object used to store the train departures.
   * @return A Result object with the attributes for the new departure.
   */
  private static Result retrieveAttributesDeparture(
      Scanner scanner, TrainDispatchRegister station) {
    int departureHour;
    if ((departureHour = TrainDispatchErrorhandling.isValidHour(scanner)) == -1) {
      return null;
    }

    int departureMinutes;
    if ((departureMinutes = TrainDispatchErrorhandling.isValidMinutes(scanner)) == -1) {
      return null;
    }

    if (LocalTime.of(departureHour, departureMinutes).isBefore(station.getClockTime())) {
      System.out.println("The departure time must be after the current time.");
      return null;
    }

    String line;
    if ((line = TrainDispatchErrorhandling.getValidLine(scanner)) == null) {
      return null;
    }

    int trainNumber;
    if ((trainNumber = TrainDispatchErrorhandling.getValidTrainNumber(scanner)) == -1) {
      return null;
    }

    String destination;
    if ((destination = TrainDispatchErrorhandling.getValidDestination(scanner)) == null) {
      return null;
    }

    destination =
        destination.substring(0, 1).toUpperCase() + destination.substring(1).toLowerCase();

    int delay;
    if ((delay = TrainDispatchErrorhandling.getValidDelay(scanner)) == -1) {
      return null;
    }

    int track = getTrackInput(scanner);
    return new Result(
        departureHour, departureMinutes, line, trainNumber, destination, delay, track);
  }

  /**
   * This method is used to ask the user if the departure has a track or not. If the user enters
   * 'y', it calls the getValidTrack method from TrainDispatchErrorhandling to validate the user
   * input. If the user enters 'n', it returns -1.
   *
   * @param scanner The scanner object used to read user input.
   * @return The track input from the user.
   */
  private static int getTrackInput(Scanner scanner) {
    System.out.print("Does the new departure have a track? (Enter 'y' for Yes or 'n' for No): ");
    String hasTrackInput = scanner.next().toLowerCase();
    return hasTrackInput.equals("y") ? TrainDispatchErrorhandling.getValidTrack(scanner) : -1;
  }

  /**
   * This method is used to print the result message of adding a new departure to the station.
   *
   * @param added The boolean value that indicates whether the departure was added or not.
   */
  private static void printAdditionResult(boolean added) {
    if (added) {
      System.out.println("The departure has been added to the station.");
    } else {
      System.out.println("The departure could not be added.");
    }
  }

  /**
   * This method is used to print a formatted overview of all the departures in the station.
   *
   * @param station The station object used to store the train departures.
   */
  private static void printOverviewOfAllDepartures(TrainDispatchRegister station) {
    if (station.size() == 0) {
      System.out.println("There are no departures in the station.");
    }
    System.out.println(station);
  }

  /**
   * This method is used to set a new track to a chosen departure. It calls the
   * getValidInputWithRetry method from TrainDispatchErrorhandling to validate the user input. It
   * catches an exception if the user enters an invalid train number or track.
   *
   * @param scanner The scanner object used to read user input.
   * @param station The station object used to store the train departures.
   */
  private static void setNewTrackToChosenDeparture(Scanner scanner, TrainDispatchRegister station) {
    try {
      Integer trainNumber1 = getTrainNumber(scanner, station);
      if (trainNumber1 == null) {
        return;
      }
      int track1 =
          TrainDispatchErrorhandling.getValidInputWithRetry(
              scanner,
              "Enter a valid track for the departure (Must be between 1-20): ",
              value -> value >= 1 && value <= 20);

      if (track1 == -1) {
        return;
      }

      station.setTrack(trainNumber1, track1);
      System.out.println(
          "The track for the departure with train number "
              + trainNumber1
              + " has been set to "
              + track1
              + ".");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This is an extracted method from the methods setTrack and setDelay, because these lines were
   * used in both methods. The method is used to get a valid train number from the user.
   *
   * @param scanner is the scanner object used to read user input.
   * @param station is the station object used to store the train departures.
   * @return the train number from the user.
   */
  private static Integer getTrainNumber(Scanner scanner, TrainDispatchRegister station) {
    System.out.println(station);
    scanner.nextLine();
    int trainNumber1 =
        TrainDispatchErrorhandling.getValidInputWithRetry(
            scanner,
            "Enter a valid train number for the departure (Must be between 1-9999): ",
            value -> value >= 1 && value <= 9999);
    if (trainNumber1 == -1) {
      return null;
    }
    scanner.nextLine();
    return trainNumber1;
  }

  /**
   * This method is used to set a delay to a chosen departure. It calls the getValidInputWithRetry
   * method from TrainDispatchErrorhandling to validate the user input. It catches an exception if
   * the user enters an invalid train number or delay.
   *
   * @param scanner The scanner object used to read user input.
   * @param station The station object used to store the train departures.
   */
  private static void setDelayThroughTrainNumber(Scanner scanner, TrainDispatchRegister station) {
    try {
      Integer trainNumber2 = getTrainNumber(scanner, station);
      if (trainNumber2 == null) {
        return;
      }
      int delay1 =
          TrainDispatchErrorhandling.getValidInputWithRetry(
              scanner,
              "Enter a valid delay for the departure (Must be between 0-59): ",
              value -> value >= 0 && value <= 59);
      if (delay1 == -1) {
        return;
      }
      station.setDelay(trainNumber2, delay1);
      System.out.println(
          "The delay for the departure with train number "
              + trainNumber2
              + " has been set to "
              + delay1
              + ".");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This method is used to search for a departure by train number. It calls the
   * getValidInputWithRetry method from TrainDispatchErrorhandling to validate the user input. It
   * catches an exception if the user enters an invalid train number.
   *
   * @param scanner The scanner object used to read user input.
   * @param station The station object used to store the train departures.
   */
  private static void searchDepartureByTrainNumber(Scanner scanner, TrainDispatchRegister station) {
    System.out.println("Here is an overview of all departures from Oslo S:");
    System.out.println(station);
    try {
      scanner.nextLine();
      int trainNumber3 =
          TrainDispatchErrorhandling.getValidInputWithRetry(
              scanner,
              "Enter a valid train number for the departure (Must be between 1-9999): ",
              value -> value >= 1 && value <= 9999);

      if (trainNumber3 != -1) {
        System.out.println(station.getDepartureByTrainNumber(trainNumber3));
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This method is used to search for a departure by destination. It calls the
   * getValidStringInputWithRetryString method from TrainDispatchErrorhandling to validate the user
   * input. It catches an exception if the user enters an invalid destination.
   *
   * @param scanner The scanner object used to read user input.
   * @param station The station object used to store the train departures.
   */
  private static void searchDepartureByDestination(Scanner scanner, TrainDispatchRegister station) {
    System.out.println("Here is an overview of all departures from Oslo S:");
    System.out.println(station);
    try {
      String destination1 =
          getValidStringInputWithRetryString(
              scanner,
              "Enter a valid destination for the departure (Must be a string): ",
              value -> value.matches("[A-ZÆØÅa-zæøå\\s\\-_]{1,50}$"));

      if (destination1 != null) {
        System.out.println(station.getDeparturesByDestination(destination1));
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This method is used to refresh the clock. It calls the getValidInputWithRetry method from
   * TrainDispatchErrorhandling to validate the user input. It catches an exception if the user
   * enters an invalid hour or minutes.
   *
   * @param scanner The scanner object used to read user input.
   * @param station The station object used to store the train departures.
   */
  private static void refreshClock(Scanner scanner, TrainDispatchRegister station) {
    try {
      scanner.nextLine();
      int hour2 =
          TrainDispatchErrorhandling.getValidInputWithRetry(
              scanner,
              "Enter a valid hour for the clock (Must be between 0-23): ",
              value -> value >= 0 && value <= 23);
      if (hour2 == -1) {
        return;
      }
      scanner.nextLine();
      int minutes2 =
          TrainDispatchErrorhandling.getValidInputWithRetry(
              scanner,
              "Enter a valid minutes for the clock (Must be between 0-59): ",
              value -> value >= 0 && value <= 59);
      station.refreshClock(hour2, minutes2);
      System.out.println("The clock is set to: " + station.getClockString());
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This method is used to display the departures in a time interval entered by the user. It calls
   * the getValidInputWithRetry method from TrainDispatchErrorhandling to validate the user input.
   * It catches an exception if the user enters an invalid hour or minutes.
   *
   * @param station The station object used to store the train departures.
   */
  public static void displayDepartureInterval(Scanner scanner, TrainDispatchRegister station) {
    try {
      System.out.println("The start time: ");
      int startHour;
      if ((startHour = TrainDispatchErrorhandling.isValidHour(scanner)) == -1) {
        return;
      }
      int startMinutes;
      if ((startMinutes = TrainDispatchErrorhandling.isValidMinutes(scanner)) == -1) {
        return;
      }

      System.out.println("\nThe end time: ");
      int endHour;
      if ((endHour = TrainDispatchErrorhandling.isValidHour(scanner)) == -1) {
        return;
      }
      int endMinutes;
      if ((endMinutes = TrainDispatchErrorhandling.isValidMinutes(scanner)) == -1) {
        return;
      }

      LocalTime startTime = LocalTime.of(startHour, startMinutes);
      LocalTime endTime = LocalTime.of(endHour, endMinutes);
      System.out.println("\nStart time: " + startTime);
      System.out.println("End time: " + endTime);
      System.out.println("\nDepartures in the specified time interval: ");

      System.out.println(station.displayDepartureInterval(startTime, endTime));

    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This method is used to print statistics about the train departures. It calls the appropriate
   * methods from TrainDispatchAnalytics.
   *
   * @param station The station object used to store the train departures.
   */
  private static void printStatistics(TrainDispatchRegister station) {
    try {
      System.out.println("Here is an overview of all departures from Oslo S:");
      System.out.println(station);
      System.out.println(
          "The average delay of all the train departures is: "
              + TrainDispatchAnalytics.calculateAverageDelay(station.getStation())
              + " minutes");
      System.out.println(
          "The most common destination(s) is: "
              + TrainDispatchAnalytics.findMostCommonDestination(station.getStation()));

      System.out.println(
          "The busiest hour of the day is: "
              + TrainDispatchAnalytics.findHourWithMostDepartures(station.getStation()));
      System.out.println(
          "The most common track number is: "
              + TrainDispatchAnalytics.findMostCommonTrackNumber(station.getStation()));
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This method is used to delete a departure by train number. It calls the getValidInputWithRetry
   * method from TrainDispatchErrorhandling to validate the user input. It catches an exception if
   * the user enters an invalid train number.
   *
   * @param scanner The scanner object used to read user input.
   * @param station The station object used to store the train departures.
   */
  private static void deleteDepartureByTrainNumber(Scanner scanner, TrainDispatchRegister station) {
    try {
      scanner.nextLine();
      System.out.println(station);
      int trainNumber4 =
          TrainDispatchErrorhandling.getValidInputWithRetry(
              scanner,
              "Enter a valid train number for the departure (Must be between 1-9999): ",
              value -> value >= 1 && value <= 9999);
      station.deleteDepartureByTrainNumber(trainNumber4);
      System.out.println("The departure with train number " + trainNumber4 + " has been deleted.");
      System.out.println("Here is an updated overview of all departures from Oslo S:");
      System.out.println(station);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * This method is used to initialize the clock and add some default departures to the station. It
   * initializes the clock by asking the user to enter the hour and minutes for the clock and checks
   * if the input is valid.
   */
  public void init() {
    System.out.println(" -- Welcome to the Train Dispatch System at Oslo S -- ");
    System.out.println(" -- Please start by initializing the system clock -- " + "\n");
    int hour;
    while (true) {
      System.out.println("Enter the hour for the clock (Must be between 0-23): ");
      if (scanner.hasNextInt()) {
        hour = scanner.nextInt();
        if (hour >= 0 && hour <= 23) {
          break;
        } else {
          System.out.println("Invalid hour. Please enter a valid hour (Must be between 0-23).");
        }
      } else {
        System.out.println("Invalid input. Please enter a valid hour (Must be between 0-23).");
        scanner.next();
      }
    }

    int minutes;
    while (true) {
      System.out.println("Enter the minutes for the clock (Must be between 0-59): ");
      if (scanner.hasNextInt()) {
        minutes = scanner.nextInt();
        if (minutes >= 0 && minutes <= 59) {
          break;
        } else {
          System.out.println("Invalid minutes. Please choose a valid time.");
        }
      } else {
        System.out.println("Invalid input. Please enter a valid minutes (Must be between 0-59).");
        scanner.next();
      }
    }

    station = new TrainDispatchRegister(hour, minutes);
    System.out.println("The clock is set to: " + station.getClockString());

    station.addDeparture(15, 29, "F5", 346, "National", 13, 3);
    station.addDeparture(12, 13, "L1", 602, "Holmlia", 1, 0);
    station.addDeparture(13, 19, "F4", 345, "National", 12, 0);
    station.addDeparture(13, 23, "L2", 603, "Moss", 2, 0);
    station.addDeparture(14, 40, "L2", 608, "Moss", 2, 0);
  }

  /** This method is used to print the menu to the user. It is initialized in the start method. */
  public void printMenu() {
    System.out.println("\nChoose an option:");
    System.out.println("1  - Add a new departure");
    System.out.println("2  - Print an overview of all departures from Oslo S");
    System.out.println("3  - Set new track to a chosen departure");
    System.out.println("4  - Set a delay to a chosen departure");
    System.out.println("5  - Search for a departure by train number");
    System.out.println("6  - Search for a departure by destination");
    System.out.println("7  - Refresh the clock");
    System.out.println("8  - Delete a departure");
    System.out.println("9  - Print statistics");
    System.out.println("10 - Display departures in a time interval");
    System.out.println("0  - Exit");
  }

  /**
   * This method is used to start the program and display the menu. It is also used to handle user
   * input for the options and call the appropriate methods.
   */
  public void start() {
    boolean running = true;
    while (running) {
      station.removeExpiredDepartures();
      station.addDelayToDuplicateDeparture();

      printMenu();
      int choice = -1;

      if (scanner.hasNextInt()) {
        choice = scanner.nextInt();
      } else {
        scanner.next();
      }

      switch (choice) {
        case 1 -> addNewDeparture(scanner, station);
        case 2 -> printOverviewOfAllDepartures(station);
        case 3 -> setNewTrackToChosenDeparture(scanner, station);
        case 4 -> setDelayThroughTrainNumber(scanner, station);
        case 5 -> searchDepartureByTrainNumber(scanner, station);
        case 6 -> searchDepartureByDestination(scanner, station);
        case 7 -> refreshClock(scanner, station);
        case 8 -> deleteDepartureByTrainNumber(scanner, station);
        case 9 -> printStatistics(station);
        case 10 -> displayDepartureInterval(scanner, station);
        case 0 -> {
          running = false;
          System.out.println("Exit");
        }
        default -> System.out.println("Invalid input. Please enter a valid option.");
      }
    }
  }

  private record Result(
      int departureHour,
      int departureMinutes,
      String line,
      int trainNumber,
      String destination,
      int delay,
      int track) {}
}
