package edu.ntnu.stud.view;

import edu.ntnu.stud.model.TrainDeparture;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to manage the colors of the train departures. The class contains a map of
 * train lines and their corresponding colors. The class also contains methods to get the color for
 * a train line, set the color for a train line, generate a random color, and get the color for a
 * train departure. Majority of the logic to retrieve the correct colors were inspired by ChatGPT,
 * except for the use of HashMaps and the getColorCodeForLighterColor method. Responsibility: To
 * manage the colors of the train departures.
 */
public class TrainDepartureColorsManager {

  /** A map of train lines and their corresponding colors. */
  private final Map<String, Color> lineColors = new HashMap<>();

  /**
   * Method to get the color for a train line.
   *
   * @param line The train line.
   * @return The color for the train line.
   */
  public Color getColorForLine(String line) {
    return lineColors.get(line);
  }

  /**
   * Method to set the color for a train line. The method uses the put method from the HashMap
   * class, that either adds a new key-value pair to the map, or replaces the value of an existing
   * key.
   *
   * @param line The train line.
   * @param color The color for the train line.
   */
  public void setColorForLine(String line, Color color) {
    lineColors.put(line, color);
  }

  /**
   * Method to generate a random color for a train line. The method uses the Math.random method to
   * generate a random number between 0 and 255 for each RGB component of the color. The method then
   * returns a new Color object with the generated RGB components.
   *
   * @return A random color.
   */
  public Color generateRandomColor() {
    int red = (int) (Math.random() * 256);
    int green = (int) (Math.random() * 256);
    int blue = (int) (Math.random() * 256);
    return new Color(red, green, blue);
  }

  /**
   * Calculates a color code for a lighter shade of the provided Color by adding an offset to its
   * RGB components. The offset helps make the color appear lighter and easier to read on the black
   * terminal. The method returns a String representing the ANSI escape code for the lighter color.
   * The method was inspired by ChatGPT.
   *
   * @param color The Color to make lighter.
   * @return A String representing the ANSI escape code for the lighter color.
   */
  public String getColorCodeForLighterColor(Color color) {
    int r = color.getRed();
    int g = color.getGreen();
    int b = color.getBlue();

    int offset = 100;

    r = Math.min(255, r + offset);
    g = Math.min(255, g + offset);
    b = Math.min(255, b + offset);

    return String.format("\u001B[38;2;%d;%d;%dm", r, g, b);
  }

  /**
   * Get the color for a train departure. If the train line has a color, the color is returned. If
   * the train line does not have a color, a random color is generated and returned. The method uses
   * the put method from the HashMap class.
   *
   * @param departure The train departure.
   * @return The color for the train departure.
   */
  public Color getColorForDeparture(TrainDeparture departure) {
    String line = departure.getLine();
    Color originalColor = lineColors.get(line);

    if (originalColor == null) {
      originalColor = generateRandomColor();
      lineColors.put(line, originalColor);
    }

    return originalColor;
  }
}
