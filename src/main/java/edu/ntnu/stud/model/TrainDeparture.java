package edu.ntnu.stud.model;

import java.time.LocalTime;

/**
 * This class represents a train departure. The class has mostly get-methods, except for the
 * setTrack nd setDelay method. They are needed to change the track and delay of a train departure
 * after it has been created. The rest of the fields are final, and can only be set in the
 * constructor. The class handles invalid input by throwing IllegalArgumentExceptions.
 * Responsibility: Represent a train departure.
 */
public class TrainDeparture {

  /** The String line of the train departure. */
  private final String line;

  /** The int train number of the train departure. */
  private final int trainNumber;

  /** The String destination of the train departure. */
  private final String destination;

  /** The LocalTime departure time of the train departure. */
  private final LocalTime departureTime;

  /** The int track or platform number of the train departure. */
  private int track;

  /** The int delay of the train departure in minutes. */
  private int delay;

  /**
   * Constructs a TrainDeparture instance by assigning the parameters as attributes. The constructor
   * also checks if the parameters are valid, and throws an IllegalArgumentException if they are
   * not. This constructor is used when the departure has a track.
   *
   * @param hour is an integer representing the hour of departure time.
   * @param minutes is an integer representing the minutes of departure time.
   * @param line is a String representing the line or route of the train departure.
   * @param trainNumber is an integer representing the unique train number of the train departure.
   * @param destination is a String representing the destination of the train departure.
   * @param track is an integer representing the track or platform number of the train departure.
   * @param delay is an integer representing the delay of the train departure in minutes.
   * @throws IllegalArgumentException If any of the parameters are outside their specifications.
   */
  public TrainDeparture(
      int hour, int minutes, String line, int trainNumber, String destination, int track, int delay)
      throws IllegalArgumentException {
    if (hour < 0 || hour > 23) {
      throw new IllegalArgumentException("Hour must be between 0 and 23");
    }
    if (minutes < 0 || minutes > 59) {
      throw new IllegalArgumentException("Minutes must be between 0 and 59");
    }
    if (trainNumber < 1 || trainNumber > 9999) {
      throw new IllegalArgumentException("Train number must be between 1 and 9999");
    }
    if (track < 1 || track > 20) {
      throw new IllegalArgumentException("Track must be between 1 and 20");
    }
    if (delay < 0 || delay > 59) {
      throw new IllegalArgumentException("Delay must be between 0 and 59");
    }
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = track;
    this.departureTime = LocalTime.of(hour, minutes);
    this.delay = delay;
  }

  /**
   * A second constructor to create a TrainDepartures instance. This constructor is used when the
   * departure does not have a track yet. The track is set to -1, and can be changed later.
   *
   * @param hour is an integer representing the hour of departure time.
   * @param minutes is an integer representing the minutes of departure time.
   * @param line is a String representing the line or route of the train departure.
   * @param trainNumber is an integer representing the unique train number of the train departure.
   * @param destination is a String representing the destination of the train departure.
   * @param delay is an integer representing the delay of the train departure in minutes.
   * @throws IllegalArgumentException If any of the parameters are outside their specifications.
   */
  public TrainDeparture(
      int hour, int minutes, String line, int trainNumber, String destination, int delay)
      throws IllegalArgumentException {
    if (hour < 0 || hour > 23) {
      throw new IllegalArgumentException("Hour must be between 0 and 23");
    }
    if (minutes < 0 || minutes > 59) {
      throw new IllegalArgumentException("Minutes must be between 0 and 59");
    }
    if (trainNumber < 1 || trainNumber > 9999) {
      throw new IllegalArgumentException("Train number must be between 1 and 9999");
    }
    if (delay < 0 || delay > 59) {
      throw new IllegalArgumentException("Delay must be between 0 and 59");
    }
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = -1;
    this.departureTime = LocalTime.of(hour, minutes);
    this.delay = delay;
  }

  /**
   * Get the departure time of the train.
   *
   * @return The departure time of the train.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Get the line of the train.
   *
   * @return the line of the train.
   */
  public String getLine() {
    return line;
  }

  /**
   * Get the unique train number of the train.
   *
   * @return the train number of the train.
   */
  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Get the destination of the train.
   *
   * @return the destination of the train.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Get the track or platform number of the train.
   *
   * @return the track or platform number of the train.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Set the track or platform number of the train. The track number must be between 1 and 20 or an
   * IllegalArgumentException is thrown.
   *
   * @param track the track number of the train to set.
   */
  public void setTrack(int track) {
    if (track < 1 || track > 20) {
      throw new IllegalArgumentException("Invalid track number. Must be between 1 and 20");
    }
    this.track = track;
  }

  /**
   * Get the delay of the train.
   *
   * @return The delay of the train.
   */
  public int getDelay() {
    return delay;
  }

  /**
   * Set the departure time of the train. The delay is added to the departure time, and this also
   * updates an already set delay. The delay must be between 0 and 59 or an IllegalArgumentException
   * is thrown.
   *
   * @param delayTime The delay time in minutes.
   */
  public void setDelay(int delayTime) throws IllegalArgumentException {
    if (delayTime < 0 || delayTime > 59) {
      throw new IllegalArgumentException("Invalid delay time. Must be between 0 and 59");
    }
    this.delay = delayTime;
  }

  /**
   * Override the equals method to check if two TrainDeparture instances are equal. Two
   * TrainDeparture instances are equal if they have the same train number, or if they have the same
   * track and departure time. The method produces true if the objects are equal, and false
   * otherwise. The method also prints a message if and why the objects are equal to better user
   * experience.
   *
   * @param o The object to compare with.
   * @return True if the objects are equal, false otherwise.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrainDeparture that = (TrainDeparture) o;
    if (this.trainNumber == that.trainNumber) {
      System.out.println("Train number already exists.");
      return true;
    }

    if (this.track == that.track && this.departureTime.equals(that.departureTime)) {
      System.out.println("Track and departure time combination already exists.");
      return true;
    }

    return false;
  }

  /**
   * Create a formatted string representation of the TrainDeparture instance. Delay converted to
   * String to be able to include in the final formatted string. If delay is 0 or negative, its
   * assigned an empty string in the formatted string.
   *
   * @return A formatted string representation of the TrainDeparture instance.
   */
  @Override
  public String toString() {
    String delayText = (delay > 0) ? String.valueOf(delay) : "";
    String trackText = (track > 0) ? String.valueOf(track) : "";

    return String.format(
        "| %11d:%02d | %5s | %12d | %11s | %5s | %5s |",
        departureTime.getHour(),
        departureTime.getMinute(),
        line,
        trainNumber,
        destination,
        delayText,
        trackText);
  }
}
