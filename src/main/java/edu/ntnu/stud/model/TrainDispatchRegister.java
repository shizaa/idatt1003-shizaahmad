package edu.ntnu.stud.model;

import edu.ntnu.stud.view.TrainDepartureColorsManager;
import java.awt.Color;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * This is the main class for the train dispatch application. A class representing a register of
 * train departures at a station. It allows you to manage and retrieve information about train
 * departures, including adding, updating, and deleting departures. It also provides a clock to
 * track the current time. The methods are non-static because they rely on instance-specific data.
 * The class handles invalid input by throwing IllegalArgumentExceptions. The use of GitHub CoPilot
 * has only been used to streamline repetitive code with logic written by the author.
 *
 * <p>Responsibility: To manage the train departures at a station.
 */
public class TrainDispatchRegister {

  /** A boolean to check if duplicates have been detected. */
  private static boolean duplicatesDetected = false;

  /**
   * An ArrayList of TrainDeparture objects. The station is a list of all the train departures in
   * the register.
   */
  private final ArrayList<TrainDeparture> station = new ArrayList<>();

  /**
   * An instance of TrainDepartureColorsManager class to manage the colors of the train departures.
   */
  private TrainDepartureColorsManager colorsManager = new TrainDepartureColorsManager();

  /** The current time at the station, manipulated by the user. */
  private LocalTime clock;

  /**
   * Initializes a new TrainDepartureRegister with the given hour and minutes as the current time.
   *
   * @param hour The hour component of the initial time.
   * @param minutes The minutes component of the initial time.
   */
  public TrainDispatchRegister(int hour, int minutes) {
    clock = LocalTime.of(hour, minutes);
  }

  /**
   * Initializes a new TrainDepartureRegister with the given LocalTime as the current time and the
   * given TrainDepartureColorsManager as the color manager.
   *
   * @param clock The initial time.
   * @param colorsManager The color manager.
   */
  public TrainDispatchRegister(LocalTime clock, TrainDepartureColorsManager colorsManager) {
    this.clock = clock;
    this.colorsManager = colorsManager;
  }

  /**
   * Retrieves a list of all train departures in the register.
   *
   * @return An array of TrainDeparture objects.
   */
  public TrainDeparture[] getStation() {
    return station.toArray(new TrainDeparture[0]);
  }

  /**
   * Adds a new train departure to the register by specifying all attributes.
   *
   * @param hour is an integer representing the hour of departure time.
   * @param minutes is an integer representing the minutes of departure time.
   * @param line is a String representing the line or route of the train departure.
   * @param trainNumber is an integer representing the unique train number of the train departure.
   * @param destination is a String representing the destination of the train departure.
   * @param track is an integer representing the track or platform number of the train departure.
   * @param delay is an integer representing the delay of the train departure in minutes.
   * @return true if the departure is added successfully, false if it already exists or could not be
   *     added due to invalid input.
   */
  public boolean addDeparture(
      int hour,
      int minutes,
      String line,
      int trainNumber,
      String destination,
      int track,
      int delay) {
    LocalTime newDepartureTime = LocalTime.of(hour, minutes);

    if (newDepartureTime.isAfter(clock)) {
      TrainDeparture newDeparture =
          new TrainDeparture(hour, minutes, line, trainNumber, destination, track, delay);
      if (colorsManager.getColorForLine(line) == null) {
        colorsManager.setColorForLine(line, colorsManager.generateRandomColor());
      }
      if (station.contains(newDeparture)) {
        return false;
      } else {
        station.add(newDeparture);
        return true;
      }
    }
    return false;
  }

  /**
   * Adds a new train departure to the register without specifying a track.
   *
   * @param hour is an integer representing the hour of departure time.
   * @param minutes is an integer representing the minutes of departure time.
   * @param line is a String representing the line or route of the train departure.
   * @param trainNumber is an integer representing the unique train number of the train departure.
   * @param destination is a String representing the destination of the train departure.
   * @param delay is an integer representing the delay of the train departure in minutes.
   * @return true if the departure is added successfully, false if it already exists.
   */
  public boolean addDepartureNoTrack(
      int hour, int minutes, String line, int trainNumber, String destination, int delay) {
    LocalTime newDepartureTime = LocalTime.of(hour, minutes);
    if (newDepartureTime.isAfter(clock)) {
      TrainDeparture newDeparture =
          new TrainDeparture(hour, minutes, line, trainNumber, destination, delay);
      if (colorsManager.getColorForLine(line) == null) {
        colorsManager.setColorForLine(line, colorsManager.generateRandomColor());
      }
      if (station.contains(newDeparture)) {
        return false;
      } else {
        station.add(newDeparture);
        return true;
      }
    }
    return false;
  }

  /**
   * Extracted method to add departures to the matchingDeparturesRegister. This was done to reduce
   * code duplication.
   *
   * @param departuresInInterval List of departures in the interval
   * @param matchingDeparturesRegister The register to add the departures to
   */
  public void addDeparturesInMatchingDepartures(
      List<TrainDeparture> departuresInInterval, TrainDispatchRegister matchingDeparturesRegister) {
    for (TrainDeparture departure : departuresInInterval) {
      if (departure.getTrack() == -1) {
        matchingDeparturesRegister.addDepartureNoTrack(
            departure.getDepartureTime().getHour(),
            departure.getDepartureTime().getMinute(),
            departure.getLine(),
            departure.getTrainNumber(),
            departure.getDestination(),
            departure.getDelay());
      } else {
        matchingDeparturesRegister.addDeparture(
            departure.getDepartureTime().getHour(),
            departure.getDepartureTime().getMinute(),
            departure.getLine(),
            departure.getTrainNumber(),
            departure.getDestination(),
            departure.getTrack(),
            departure.getDelay());
      }
    }
  }

  /**
   * Sets the track for a train departure based on its train number. The method converts the station
   * list to a stream. It can then be used for stream operations, such as filtering. The method uses
   * the filter method to filter the stream to only include the train departure with the specified
   * train number. The method then uses the findFirst method to retrieve the first train departure
   * in the stream. If match is found, the method returns an Optional containing the train
   * departure. This is the logic that was written by the author, but streamlined by GitHub CoPilot.
   *
   * @param trainNumber The train number.
   * @param track The track number to set.
   * @throws IllegalArgumentException if the train number is not between 1 and 9999, or the track is
   *     not between 1 and 20.
   */
  public void setTrack(int trainNumber, int track) throws IllegalArgumentException {
    if (trainNumber < 1 || trainNumber > 9999) {
      throw new IllegalArgumentException("Train number must be between 1 and 9999");
    }
    if (track < -1 || track > 20) {
      throw new IllegalArgumentException("Track must be between 1 and 20");
    }

    Optional<TrainDeparture> found =
        station.stream().filter(departure -> departure.getTrainNumber() == trainNumber).findFirst();
    if (found.isPresent()) {
      TrainDeparture trainDeparture = found.get();
      trainDeparture.setTrack(track);
    } else {
      throw new IllegalArgumentException("Train number not found in the station.");
    }
  }

  /**
   * Adds delay to a train departure based on its train number.
   *
   * @param trainNumber The train number.
   * @param delay The delay to add in minutes.
   * @throws IllegalArgumentException if the train number is not between 1 and 9999, or the delay is
   *     not between 0 and 59.
   */
  public void setDelay(int trainNumber, int delay) throws IllegalArgumentException {
    if (trainNumber < 1 || trainNumber > 9999) {
      throw new IllegalArgumentException("Train number must be between 1 and 9999");
    }
    if (delay < 0 || delay > 59) {
      throw new IllegalArgumentException("Delay must be between 0 and 59");
    }
    Optional<TrainDeparture> found =
        station.stream().filter(departure -> departure.getTrainNumber() == trainNumber).findFirst();
    if (found.isPresent()) {
      TrainDeparture trainDeparture = found.get();
      trainDeparture.setDelay(delay);
    } else {
      throw new IllegalArgumentException(
          "Train number " + trainNumber + " not found in the station.");
    }
  }

  /**
   * Retrieves the train departure with the specified train number. The method returns a list even
   * though there is only one departure with the specified train number. This is because of easier
   * and consistent color handling.
   *
   * @param trainNumber The train number of the departure to update.
   * @return the found departure if the track is successfully updated, null if the train is not
   *     found.
   * @throws IllegalArgumentException if the train number is not between 1 and 9999, if the track
   *     number is not between 1 and 20, or if the train number is not found in the station.
   */
  public TrainDispatchRegister getDepartureByTrainNumber(int trainNumber)
      throws IllegalArgumentException {
    if (trainNumber <= 0 || trainNumber > 99999) {
      throw new IllegalArgumentException("Train number must be between 1 and 9999");
    }

    List<TrainDeparture> matchingDepartures =
        station.stream().filter(departure -> departure.getTrainNumber() == trainNumber).toList();
    if (matchingDepartures.isEmpty()) {
      throw new IllegalArgumentException("No departures found for the specified train number.");
    }

    TrainDispatchRegister matchingDeparturesRegister =
        new TrainDispatchRegister(this.clock, colorsManager);

    Color originalColor = colorsManager.getColorForDeparture(matchingDepartures.get(0));
    colorsManager.setColorForLine(matchingDepartures.get(0).getLine(), originalColor);

    addDeparturesInMatchingDepartures(matchingDepartures, matchingDeparturesRegister);
    return matchingDeparturesRegister;
  }

  /**
   * Retrieves a list of train departures with the specified destination.
   *
   * @param destination The destination to filter departures.
   * @return An ArrayList of TrainDeparture objects with the specified destination.
   * @throws IllegalArgumentException if the destination string length exceeds 50 characters, if the
   *     destination contains invalid characters and if no departures are found for the specified
   *     destination.
   */
  public TrainDispatchRegister getDeparturesByDestination(String destination)
      throws IllegalArgumentException {
    destination =
        destination.substring(0, 1).toUpperCase() + destination.substring(1).toLowerCase();
    if (!destination.matches("[A-ZÆØÅa-zæøå\\s\\-_]{1,41}$")) {
      throw new IllegalArgumentException(
          "Destination must be a string between 1 and 50 characters");
    }

    String finalDestination = destination;
    List<TrainDeparture> matchingDepartures =
        station.stream()
            .filter(departure -> departure.getDestination().equals(finalDestination))
            .toList();
    if (matchingDepartures.isEmpty()) {
      throw new IllegalArgumentException("No departures found for the specified destination.");
    }

    TrainDispatchRegister matchingDeparturesRegister =
        new TrainDispatchRegister(this.clock, colorsManager);

    Color originalColor = colorsManager.getColorForDeparture(matchingDepartures.get(0));
    colorsManager.setColorForLine(matchingDepartures.get(0).getLine(), originalColor);

    addDeparturesInMatchingDepartures(matchingDepartures, matchingDeparturesRegister);
    return matchingDeparturesRegister;
  }

  /**
   * Refreshes the clock to the specified hour and minutes. Try catch for invalid time. If the hour
   * and minutes are invalid, the clock is not updated.
   *
   * @param hour The new hour for the clock.
   * @param minutes The new minutes for the clock.
   */
  public void refreshClock(int hour, int minutes) {
    LocalTime newClock = LocalTime.of(hour, minutes);
    if (newClock.isBefore(clock)) {
      throw new IllegalArgumentException(
          "Invalid time. The clock cannot be set to a time in the past.");
    } else {
      clock = newClock;
    }
  }

  /**
   * Deletes a train departure by its train number.
   *
   * @param trainNumber The train number to delete.
   * @throws IllegalArgumentException if the train number is not between 1 and 9999 or if the train
   *     number is not found in the station.
   */
  public void deleteDepartureByTrainNumber(int trainNumber) throws IllegalArgumentException {
    if (trainNumber < 1 || trainNumber > 9999) {
      throw new IllegalArgumentException("Train number must be between 1 and 9999");
    }
    Optional<TrainDeparture> found =
        station.stream().filter(departure -> departure.getTrainNumber() == trainNumber).findFirst();
    if (found.isPresent()) {
      station.remove(found.get());
    } else {
      throw new IllegalArgumentException("Train number not found in the station.");
    }
  }

  /**
   * Displays all departures within a time interval chosen by the user.
   *
   * @param startTime The hour and minutes of the start time
   * @param endTime The hour and minutes of the end time
   * @return A TrainDepartureRegister list containing all departures within the specified time
   *     interval.
   * @throws IllegalArgumentException if the start time or end time is null, or if the start time is
   *     after the end time.
   */
  public TrainDispatchRegister displayDepartureInterval(LocalTime startTime, LocalTime endTime)
      throws IllegalArgumentException {
    List<TrainDeparture> departuresInInterval =
        station.stream()
            .filter(
                departure ->
                    departure.getDepartureTime().isAfter(startTime)
                        && departure.getDepartureTime().isBefore(endTime))
            .toList();

    if (departuresInInterval.isEmpty()) {
      throw new IllegalArgumentException("No departures found within the specified time range.");
    }

    TrainDispatchRegister matchingDeparturesRegister =
        new TrainDispatchRegister(this.clock, colorsManager);

    Color originalColor = colorsManager.getColorForDeparture(departuresInInterval.get(0));
    colorsManager.setColorForLine(departuresInInterval.get(0).getLine(), originalColor);

    addDeparturesInMatchingDepartures(departuresInInterval, matchingDeparturesRegister);

    return matchingDeparturesRegister;
  }

  /**
   * Gets the number of train departures in the register.
   *
   * @return The number of train departures in the register.
   */
  public int size() {
    return station.size();
  }

  /**
   * Retrieves a string representation of the current clock time.
   *
   * @return A string representation of the current clock time.
   */
  public String getClockString() {
    return clock.toString();
  }

  /**
   * A method that automatically ads delay to a departure that has the same track and departure time
   * of another departure. The delay is added in increments of 3 minutes. This method is called when
   * the user tries to add a departure with the same track and departure time as another departure.
   */
  public void addDelayToDuplicateDeparture() {
    List<TrainDeparture> matchingDepartures = new ArrayList<>();
    for (TrainDeparture departure : station) {
      for (TrainDeparture departure2 : station) {
        if (departure.getTrack() == departure2.getTrack()
            && departure.getDepartureTime().equals(departure2.getDepartureTime())
            && departure.getTrainNumber() != departure2.getTrainNumber()) {
          matchingDepartures.add(departure);
          if (!duplicatesDetected) {
            System.out.println("Added delay because of duplicate departures.");
            duplicatesDetected = true;
          }
        }
      }
    }

    if (!matchingDepartures.isEmpty()) {
      int delay = 0;
      for (TrainDeparture matchingDeparture : matchingDepartures) {
        matchingDeparture.setDelay(delay);
        delay += 3;
      }
    }
  }


  /** Removes departures that have already departed based on the specified hour and minutes. */
  public void removeExpiredDepartures() {
    LocalTime currentTime = getClockTime();
    station.removeIf(
        departure ->
            departure.getDepartureTime().plusMinutes(departure.getDelay()).isBefore(currentTime));
  }

  /**
   * Retrieves the current time.
   *
   * @return The current time.
   */
  public LocalTime getClockTime() {
    return clock;
  }

  /**
   * Retrieves the track of the first train departure in the register. This method is necessary for
   * the tests.
   *
   * @return The track of the first train departure in the register.
   */
  public int getTrack() {
    return station.get(0).getTrack();
  }

  /**
   * Generates a string representation of a train departure. If the track is -1, the track is
   * colored red and displayed as "n/a". If the delay is greater than 0, the delay is colored
   * yellow. The line is colored according to the color of the line. The method uses ANSI escape
   * codes to color the text.
   *
   * @param departure The train departure to generate a string representation of.
   * @param lineColorCode The ANSI escape code for the color of the train line.
   * @return A formatted string representing the train departure.
   */
  public String getString(TrainDeparture departure, String lineColorCode) {
    String colorReset = "\u001B[0m";

    int timeColumnWidth = 18;
    int lineColumnWidth = 8;
    int trainNumberColumnWidth = 16;
    int destinationColumnWidth = 42;
    int delayColumnWidth = 9;
    int trackColumnWidth = 9;

    if (departure.getTrack() == -1) {
      trackColumnWidth += 9;
    }

    return String.format(
        "| %-"
            + timeColumnWidth
            + "s | %s%-"
            + lineColumnWidth
            + "s%s | %-"
            + trainNumberColumnWidth
            + "d | %-"
            + destinationColumnWidth
            + "s | %s%-"
            + delayColumnWidth
            + "s%s | %-"
            + trackColumnWidth
            + "s |\n",
        String.format(
            "%02d:%02d",
            departure.getDepartureTime().getHour(), departure.getDepartureTime().getMinute()),
        lineColorCode,
        departure.getLine(),
        colorReset,
        departure.getTrainNumber(),
        departure.getDestination(),
        (departure.getDelay() > 0) ? "\u001B[33m" : "",
        (departure.getDelay() > 0) ? String.valueOf(departure.getDelay()) : "",
        (departure.getDelay() > 0) ? colorReset : "",
        (departure.getTrack() == -1) ? "\u001B[31m" + "n/a" + colorReset : departure.getTrack());
  }

  /**
   * Generates a string representation of the train departures in the register. It sorts the
   * departures by departure time before generating the string. The method uses the getString method
   * to generate a string representation of each train departure.
   *
   * @param includeColors Whether to include colors in the string representation. This is included
   *     because the tests do not support ANSI escape codes, especially when they are randomly
   *     generated for each use. Therefore, to test the toString, it is sent with the parameter
   *     false. When the application is run, the parameter is true.
   * @return A formatted string representing the train departures in the register.
   */
  public String toString(boolean includeColors) {
    StringBuilder sb = new StringBuilder();
    sb.append("Current time: ").append(this.clock).append("\n");
    sb.append(
        "--------------------------------------------------------------"
            + "-----------------------------------------------------------"
            + "\n");
    sb.append(
        "|   Departure Time   |   Line   |   Train Number   |          "
            + "       Destination                |   Delay   |   Track   |"
            + "\n");
    sb.append(
        "--------------------------------------------------------------"
            + "-----------------------------------------------------------"
            + "\n");

    List<TrainDeparture> sortedDepartures = new ArrayList<>(station);
    sortedDepartures.sort(Comparator.comparing(TrainDeparture::getDepartureTime));
    for (TrainDeparture departure : sortedDepartures) {
      String lineColorCode =
          includeColors
              ? colorsManager.getColorCodeForLighterColor(
                  colorsManager.getColorForLine(departure.getLine()))
              : "";
      String formattedDeparture = getString(departure, lineColorCode);

      sb.append(formattedDeparture);
    }

    sb.append(
        "--------------------------------------------------------------------"
            + "-----------------------------------------------------");

    return sb.toString();
  }

  /**
   * The overridden toString method. It calls the toString method with the parameter true to include
   * colors.
   */
  @Override
  public String toString() {
    return toString(true);
  }
}
