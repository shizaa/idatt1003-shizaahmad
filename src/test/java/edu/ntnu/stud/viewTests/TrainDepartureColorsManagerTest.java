package edu.ntnu.stud.viewTests;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.view.TrainDepartureColorsManager;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.awt.Color;
import static org.junit.jupiter.api.Assertions.*;

class TrainDepartureColorsManagerTest {

  @Test
  @DisplayName("Test if the method returns the color for a train line")
  void getColorForLine() {
    TrainDepartureColorsManager manager = new TrainDepartureColorsManager();
    String line = "TestLine";
    Color color = new Color(255, 0, 0);

    manager.setColorForLine(line, color);

    assertEquals(color, manager.getColorForLine(line));
  }

  @Test
  @DisplayName("Test if the method sets the color for a train line")
  void setColorForLine() {
    TrainDepartureColorsManager manager = new TrainDepartureColorsManager();
    String line = "TestLine";
    Color color = new Color(0, 255, 0);

    manager.setColorForLine(line, color);

    assertEquals(color, manager.getColorForLine(line));
  }

  @Test
  @DisplayName("Test if the method generates a random color")
  void generateRandomColor() {
    TrainDepartureColorsManager manager = new TrainDepartureColorsManager();
    Color randomColor1 = manager.generateRandomColor();
    Color randomColor2 = manager.generateRandomColor();

    assertNotEquals(randomColor1, randomColor2);
  }

  @Test
  @DisplayName("Test if the method calculates a color code for a lighter shade")
  void getColorCodeForLighterColor() {
    TrainDepartureColorsManager manager = new TrainDepartureColorsManager();
    Color originalColor = new Color(0, 0, 255);

    String lighterColorCode = manager.getColorCodeForLighterColor(originalColor);

    assertNotNull(lighterColorCode);
  }

  @Test
  @DisplayName("Test if the method returns the color for a train departure")
  void getColorForDeparture() {
    TrainDepartureColorsManager manager = new TrainDepartureColorsManager();
    TrainDeparture departure =
        new TrainDeparture(12, 30, "TestLine", 1234, "TestDestination", 5, 0);

    Color color = manager.getColorForDeparture(departure);

    assertNotNull(color);
  }
}
