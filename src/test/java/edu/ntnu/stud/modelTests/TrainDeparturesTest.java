package edu.ntnu.stud.modelTests;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.model.TrainDeparture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Test class for the TrainDeparture class. The class tests the constructor, the toString method,
 * the equals method, the setDelay method and the setTrack method. These are the methods with
 * functionality. The other methods are getters and are not tested. The class has nested classes for
 * the different constructors and the equals method. This is to make the tests more readable and to
 * separate the different tests. Responsibility: To test the TrainDeparture class.
 */
class TrainDeparturesTest {

  private TrainDeparture trainDepartures;

  @BeforeEach
  @DisplayName("Set up the test")
  void setUp() {
    trainDepartures = new TrainDeparture(12, 30, "L1", 1234, "Oslo", 1, 0);
  }
  @Nested
  @DisplayName("Constructor Tests for the first constructor")
  class ConstructorTests {

    @Test
    @DisplayName("Valid constructor parameters")
    void validConstructorParameters() {
      assertDoesNotThrow(() -> new TrainDeparture(12, 30, "L1", 1234, "Oslo", 1, 0));
    }

    @Test
    @DisplayName("Invalid hour in constructor")
    void invalidHourInConstructor() {
      try {
        new TrainDeparture(-1, 30, "L1", 1234, "Oslo", 1, 0);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Hour must be between 0 and 23", e.getMessage());
      }
    }

    @Test
    @DisplayName("Invalid minutes in constructor")
    void invalidMinutesInConstructor() {
      try {
        new TrainDeparture(12, 60, "L1", 1234, "Oslo", 1, 0);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Minutes must be between 0 and 59", e.getMessage());
      }
    }

    @Test
    @DisplayName("Invalid train number in constructor")
    void invalidTrainNumberInConstructor() {
      try {
        new TrainDeparture(12, 30, "L1", 10000, "Oslo", 1, 0);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Train number must be between 1 and 9999", e.getMessage());
      }
    }

    @Test
    @DisplayName("Invalid track in constructor")
    void invalidTrackInConstructor() {
      try {
        new TrainDeparture(12, 30, "L1", 1234, "Oslo", 21, 0);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Track must be between 1 and 20", e.getMessage());
      }
    }

    @Test
    @DisplayName("Invalid delay in constructor")
    void invalidDelayInConstructor() {
      try {
        new TrainDeparture(12, 30, "L1", 1234, "Oslo", 1, 60);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Delay must be between 0 and 59", e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Constructor Tests for only the track logic in the second constructor")
  class ConstructorTests2 {
    @Test
    @DisplayName("Valid constructor parameters with track")
    void validConstructorParametersWithTrack() {
      assertDoesNotThrow(() -> new TrainDeparture(12, 30, "L1", 1234, "Oslo", 1, 0));
    }

    @Test
    @DisplayName("Constructor sets track to -1 when not provided")
    void trackIsSetToMinusOneWhenNotProvided() {
      TrainDeparture departure = new TrainDeparture(12, 30, "L1", 1234, "Oslo", 0);
      assertEquals(-1, departure.getTrack());
    }
    @Test
    @DisplayName("Invalid hour in constructor")
    void invalidHourInConstructor() {
      try {
        new TrainDeparture(-1, 30, "L1", 1234, "Oslo",  0);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Hour must be between 0 and 23", e.getMessage());
      }
    }

    @Test
    @DisplayName("Invalid minutes in constructor")
    void invalidMinutesInConstructor() {
      try {
        new TrainDeparture(12, 60, "L1", 1234, "Oslo", 10);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Minutes must be between 0 and 59", e.getMessage());
      }
    }

    @Test
    @DisplayName("Invalid train number in constructor")
    void invalidTrainNumberInConstructor() {
      try {
        new TrainDeparture(12, 30, "L1", 10000, "Oslo",  0);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Train number must be between 1 and 9999", e.getMessage());
      }
    }

    @Test
    @DisplayName("Invalid delay in constructor")
    void invalidDelayInConstructor() {
      try {
        new TrainDeparture(12, 30, "L1", 1234, "Oslo",  60);
        fail("Expected an IllegalArgumentException to be thrown");
      } catch (IllegalArgumentException e) {
        assertEquals("Delay must be between 0 and 59", e.getMessage());
      }
    }
  }

  @Test
  @DisplayName("Test if the method returns the correct string")
  void toStringTest() {
    String expected = "|          12:30 |    L1 |         1234 |        Oslo |       |     1 |";
    String actual = trainDepartures.toString();
    assertEquals(expected, actual);
  }

  @Test
  @DisplayName("Set delay with positive value")
  void setDelayPositiveValue() {
    trainDepartures.setDelay(5);
    assertEquals(5, trainDepartures.getDelay());
  }

  @Test
  @DisplayName("Set delay with negative value")
  void setDelayNegativeValue() {
    assertThrows(IllegalArgumentException.class, () -> trainDepartures.setDelay(-1));
  }

  @Nested
  @DisplayName("Tests for the set track method")
  class SetTrackTests{
    @Test
    @DisplayName("Set track")
    void setTrack() {
      trainDepartures.setTrack(2);
      assertEquals(2, trainDepartures.getTrack());
    }

    @Test
    @DisplayName("Test if the method throws for invalid track number")
    void setTrackInvalidTrackNumber() {
      assertThrows(IllegalArgumentException.class, () -> trainDepartures.setTrack(21));
    }
  }

  @Nested
  @DisplayName("Tests for the equals method")
  class EqualsTests {
    @Test
    @DisplayName("Objects are equal with the same train number.")
    void objectsAreEqualWithSameTrainNumber() {
      TrainDeparture other = new TrainDeparture(12, 30, "L2", 1234, "Oslo", 1, 0);
      assertEquals(trainDepartures, other);
    }

    @Test
    @DisplayName("Objects are equal with the same track and departure time.")
    void objectsAreEqualWithSameTrackAndDepartureTime() {
      TrainDeparture other = new TrainDeparture(12, 30, "L1", 5678, "Oslo", 1, 0);
      assertEquals(trainDepartures, other);
    }

    @Test
    @DisplayName("Objects are not equal")
    void objectsAreNotEqual() {
      TrainDeparture other = new TrainDeparture(12, 30, "L2", 5678, "Bergen", 3, 0);
      assertNotEquals(trainDepartures, other);
    }

    @Test
    @DisplayName("Test equals with null object")
    void testEqualsWithNullObject() {
      TrainDeparture departure = new TrainDeparture(12, 30, "L2", 5678, "Bergen", 3, 0);
      assertFalse(departure.equals(null));
    }

  }
}
