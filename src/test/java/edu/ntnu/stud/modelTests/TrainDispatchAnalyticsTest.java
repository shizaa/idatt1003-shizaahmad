package edu.ntnu.stud.modelTests;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.model.TrainDispatchAnalytics;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import java.util.List;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for the TrainDispatchAnalytics class.
 * All the methods in the class are tested, because they all have functionality.
 * The class contains nested classes to separate the different tests.
 * Responsibility: To test the TrainDispatchAnalytics class.
 */

class TrainDispatchAnalyticsTest {
  private List<TrainDeparture> departures;

  @BeforeEach
  @DisplayName("Set up the test")
  void setUp() {
    departures =
        Arrays.asList(
            new TrainDeparture(12, 0, "L1", 601, "Oslo", 2, 5),
            new TrainDeparture(13, 0, "L1", 604, "Oslo", 2, 5),
            new TrainDeparture(12, 30, "L2", 602, "Trondheim", 1, 0),
            new TrainDeparture(13, 0, "L1", 603, "Oslo", 3, 10));
  }

  @Test
  @DisplayName("Test if the method calculates the average delay")
  void calculateAverageDelay(){
    String averageDelay =
        TrainDispatchAnalytics.calculateAverageDelay(departures.toArray(new TrainDeparture[0]));

    assertEquals("5,00", averageDelay);
  }

  @Nested
  @DisplayName("Tests for the findMostCommonDestination method")
  class findMostCommonDestinationTests {
    @Test
    @DisplayName("Test if the method finds the most common destination")
    void findMostCommonDestination() {
      String mostCommonDestination =
          TrainDispatchAnalytics.findMostCommonDestination(
              departures.toArray(new TrainDeparture[0]));

      assertEquals("Oslo", mostCommonDestination);
    }

    @Test
    @DisplayName(
        "Test if the method throws exception when there are no departures with more than one destination")
    void findMostCommonDestinationWithNoDuplicateDestinations() {
      List<TrainDeparture> departuresWithNoDuplicateDestinations =
          Arrays.asList(
              new TrainDeparture(12, 0, "L1", 601, "Stavanger", 2, 5),
              new TrainDeparture(13, 0, "L1", 604, "Oslo", 2, 5),
              new TrainDeparture(12, 30, "L2", 602, "Trondheim", 1, 0),
              new TrainDeparture(13, 0, "L1", 603, "Bergen", 3, 10));

      assertThrows(
          IllegalArgumentException.class,
          () ->
              TrainDispatchAnalytics.findMostCommonDestination(
                  departuresWithNoDuplicateDestinations.toArray(new TrainDeparture[0])));
    }
  }

  @Nested
  @DisplayName("Tests for the findHourWithMostDepartures method")
  class findHourWithMostDeparturesTests {
    @Test
    @DisplayName("Test if the method finds the hour with most departures")
    void findHourWithMostDepartures() {
      int hourWithMostDepartures =
          TrainDispatchAnalytics.findHourWithMostDepartures(
              departures.toArray(new TrainDeparture[0]));

      assertEquals(12, hourWithMostDepartures);
    }

    @Test
    @DisplayName(
        "Test if the method throws exception when there are no departures with more than one hour")
    void findHourWithMostDeparturesWithNoDuplicateHours() {
      List<TrainDeparture> departuresWithNoDuplicateHours =
          Arrays.asList(
              new TrainDeparture(16, 0, "L1", 601, "Stavanger", 2, 5),
              new TrainDeparture(13, 0, "L1", 604, "Bergen", 2, 5),
              new TrainDeparture(12, 30, "L2", 602, "Trondheim", 1, 0),
              new TrainDeparture(15, 0, "L1", 603, "Oslo", 3, 10));

      assertThrows(
          IllegalArgumentException.class,
          () ->
              TrainDispatchAnalytics.findHourWithMostDepartures(
                  departuresWithNoDuplicateHours.toArray(new TrainDeparture[0])));
    }
  }

  @Nested
  @DisplayName("Tests for the mostcommontracknumber method")
  class MostCommonTrackNumberTests {
    @Test
    @DisplayName("Test if the method finds the most common track number")
    void findMostCommonTrackNumberWithDuplicateTracks() {
      int mostCommonTrackNumber =
          TrainDispatchAnalytics.findMostCommonTrackNumber(
              departures.toArray(new TrainDeparture[0]));

      assertEquals(2, mostCommonTrackNumber);
    }

    @Test
    @DisplayName(
        "Test if the method throws exception when there are no departures with more than one trakc")
    void findMostCommonTrackNumberWithNoDuplicateTracks() {
      List<TrainDeparture> departuresWithNoDuplicateTracks =
          Arrays.asList(
              new TrainDeparture(12, 0, "L1", 601, "Oslo", 2, 5),
              new TrainDeparture(13, 0, "L1", 604, "Oslo", 3, 5),
              new TrainDeparture(12, 30, "L2", 602, "Trondheim", 1, 0),
              new TrainDeparture(13, 0, "L1", 603, "Oslo", 6, 10));

      assertThrows(
          IllegalArgumentException.class,
          () ->
              TrainDispatchAnalytics.findMostCommonTrackNumber(
                  departuresWithNoDuplicateTracks.toArray(new TrainDeparture[0])));
    }
  }
}
