package edu.ntnu.stud.modelTests;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.model.TrainDispatchRegister;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

import java.time.LocalTime;
import static org.junit.jupiter.api.Assertions.*;

/**
 * This is a test class for the TrainDispatchRegister class. The class tests all the methods with
 * major functionality in the class. The class has nested classes for the different methods. This is
 * to make the tests more readable and to separate the different tests. The tests are written with
 * the help of AAA (Arrange, Act, Assert). The logic was implemented by the programmer. However,
 * after the implementation of one method, similar methods were automatically generated with the
 * help of GitHub CoPilot. Due to the randomized generating of linecolors, the tests for the
 * getString method with multiple departures are not included in the test coverage. Responsibility:
 * To test the TrainDispatchRegister class.
 */
class TrainDispatchRegisterTest {
  private TrainDispatchRegister trainDispatchRegister;

  @BeforeEach
  @DisplayName("Set up the test")
  void setUp() {
    trainDispatchRegister = new TrainDispatchRegister(12, 0);
  }

  @Test
  @DisplayName("Test getStation method")
  void testGetStation() {
    trainDispatchRegister.addDeparture(12, 10, "L1", 624, "Oslo", 2, 5);

    TrainDeparture[] station = trainDispatchRegister.getStation();

    assertEquals(1, station.length);
    assertEquals(624, station[0].getTrainNumber());
  }

  @Test
  @DisplayName("Test if the method deletes expired departures")
  void testRemoveExpiredDeparturesWithValidTime() {
    trainDispatchRegister.addDeparture(12, 13, "L2", 614, "Holmlia", 4, 13);
    trainDispatchRegister.refreshClock(12, 34);
    trainDispatchRegister.removeExpiredDepartures();
    assertEquals(0, trainDispatchRegister.size());
  }

  @Nested
  @DisplayName("Add Departure Tests for the method with no track")
  class AddDepartureTests {

    @Test
    @DisplayName("Add departure with track")
    void addDepartureWithTrack() {
      assertTrue(trainDispatchRegister.addDepartureNoTrack(12, 40, "L2", 5678, "Bergen", 3));
    }

    @Test
    @DisplayName("Add departure without track")
    void addDepartureWithoutTrack() {
      assertTrue(trainDispatchRegister.addDepartureNoTrack(12, 45, "L3", 9012, "Trondheim", 0));
    }

    @Test
    @DisplayName("Test if method returns false when departure already exists")
    void testAddDepartureWithDuplicateDepartureNoTrack() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 615, "Holmlia", 4, 13);
      assertFalse(trainDispatchRegister.addDepartureNoTrack(12, 13, "L2", 615, "Holmlia", 4));
    }

    @Test
    @DisplayName("Test if method returns false when departure already exists")
    void testAddDepartureWithDuplicateDepartureTrack() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 615, "Holmlia", 4, 2);
      assertFalse(trainDispatchRegister.addDeparture(12, 13, "L2", 615, "Holmlia", 4, 2));
    }
  }

  @Nested
  @DisplayName("Set track tests")
  class SetTrackTests {
    @Test
    @DisplayName(
        "Test if the method throws exception when trying to set track for a non-existent departure")
    void testSetTrackWithNonExistentTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 615, "Holmlia", 4, 13);
      assertThrows(IllegalArgumentException.class, () -> trainDispatchRegister.setTrack(708, 10));
    }

    @Test
    @DisplayName(
        "Test if method throws exception when trying to set track with invalid trainnumber")
    void testSetTrackWithInvalidTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 615, "Holmlia", 4, 13);
      assertThrows(IllegalArgumentException.class, () -> trainDispatchRegister.setTrack(-2, 21));
    }

    @Test
    @DisplayName(
        "Test if the method throws exception when trying to set track for a departure with a invalid track")
    void testSetTrackWithInvalidTrack() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 615, "Holmlia", 4, 13);
      assertThrows(IllegalArgumentException.class, () -> trainDispatchRegister.setTrack(615, 21));
    }

    @Test
    @DisplayName("Test if the method sets track for a departure")
    void testSetTrackWithValidInput() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 615, "Holmlia", 4, 13);
      trainDispatchRegister.setTrack(615, 19);
      assertEquals(19, trainDispatchRegister.getDepartureByTrainNumber(615).getTrack());
    }
  }

  @Nested
  @DisplayName("Set delay tests")
  class SetDelayTests {
    @Test
    @DisplayName("Test if the method sets a delay to a departure")
    void testAddDelayWithValidInput() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 617, "Holmlia", 4, 13);
      trainDispatchRegister.setDelay(617, 10);
      assertDoesNotThrow(() -> trainDispatchRegister.setDelay(617, 10));
    }

    @Test
    @DisplayName("Test if the method throws exception when trying to set a negative delay")
    void testAddDelayWithNegativeDelay() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 618, "Holmlia", 4, 13);
      assertThrows(IllegalArgumentException.class, () -> trainDispatchRegister.setDelay(619, -10));
    }

    @Test
    @DisplayName(
        "Test if the method throws exception when trying to set delay to a non-existent departure")
    void testAddDelayWithNonExistentDeparture() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 618, "Holmlia", 4, 13);
      assertThrows(IllegalArgumentException.class, () -> trainDispatchRegister.setDelay(619, 10));
    }

    @Test
    @DisplayName(
        "Test if the method throws exception when trying to set delay to a departure with invalid trainnumber")
    void testAddDelayWithInvalidTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 618, "Holmlia", 4, 13);
      assertThrows(IllegalArgumentException.class, () -> trainDispatchRegister.setDelay(-2, 10));
    }
  }

  @Nested
  @DisplayName("Get departure by train number tests")
  class GetDepartureByTrainNumberTests {
    @Test
    @DisplayName("Test if the method finds a departure with the same trainNumber")
    void testGetDepartureByTrainNumberWithValidTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L1", 608, "Holmlia", 1, 0);
      assertNotNull(trainDispatchRegister.getDepartureByTrainNumber(608));
    }

    @Test
    @DisplayName("Test if the method returns null for a non-existent departure")
    void testGetDepartureByTrainNumberWithNonExistentTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L1", 610, "Holmlia", 1, 0);
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDispatchRegister.getDepartureByTrainNumber(609));
    }

    @Test
    @DisplayName("Test if the method throws exception for a departure with invalid trainnumber")
    void testGetDepartureByTrainNumberWithInvalidTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L1", 610, "Holmlia", 1, 0);
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDispatchRegister.getDepartureByTrainNumber(-2));
    }

    @Test
    @DisplayName("Test if the method handles departure with no tracks")
    void testGetDepartureByTrainNumberWithNoTracks() {
      trainDispatchRegister.addDepartureNoTrack(12, 13, "L1", 610, "Holmlia", 1);
      assertNotNull(trainDispatchRegister.getDepartureByTrainNumber(610));
    }
  }

  @Nested
  @DisplayName("Get departure by destination tests")
  class GetDepartureByDestinationsTests {
    @Test
    @DisplayName("Test if the method returns an Arraylist of departures with the same destination")
    void testGetDepartureByDestinationWithExistentDeparture() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 611, "Holmlia", 4, 13);
      trainDispatchRegister.addDeparture(12, 3, "L1", 612, "Holmlia", 1, 0);
      assertEquals(2, trainDispatchRegister.getDeparturesByDestination("Holmlia").size());
    }

    @Test
    @DisplayName("Test if the method throws an exception for a non-existent destination")
    void testGetDepartureByDestinationWithNonExistentDeparture() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 613, "Holmlia", 4, 13);
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDispatchRegister.getDeparturesByDestination("Oslo"));
    }

    @Test
    @DisplayName("Test if the method throws exception for a departure with invalid destination")
    void testGetDepartureByDestinationWithInvalidDestination() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 613, "Holmlia", 4, 13);
      assertThrows(
          IllegalArgumentException.class,
          () ->
              trainDispatchRegister.getDeparturesByDestination(
                  "HeiDetteer en test forveldig langDestinasjon"));
    }
  }

  @Nested
  @DisplayName("Refresh clock tests and get clock string test")
  class ClockTests {
    @Test
    @DisplayName("Test if the method refreshes the clock")
    void testRefreshClockWithValidClockTime() {
      trainDispatchRegister.refreshClock(12, 34);
      assertEquals("12:34", trainDispatchRegister.getClockString());
    }

    @Test
    @DisplayName("Test if the method returns the clock as a string")
    void testGetClockString() {
      assertEquals("12:00", trainDispatchRegister.getClockString());
    }

    @Test
    @DisplayName("Test if the method throws for time in the past")
    void testRefreshClockWithTimeInPast() {
      trainDispatchRegister.refreshClock(12, 30);

      IllegalArgumentException exception =
          assertThrows(
              IllegalArgumentException.class, () -> trainDispatchRegister.refreshClock(12, 15));

      assertEquals(
          "Invalid time. The clock cannot be set to a time in the past.", exception.getMessage());
    }
  }

  @Nested
  @DisplayName("Delete departure by train number tests")
  class DeleteDepartureByTrainNumberTests {
    @Test
    @DisplayName("Test if the method deletes a departure by train number")
    void testDeleteDepartureByTrainNumberWithValidTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 620, "Holmlia", 4, 13);
      trainDispatchRegister.deleteDepartureByTrainNumber(620);
      assertEquals(0, trainDispatchRegister.size());
    }

    @Test
    @DisplayName("Test if the method throws when trying to delete a non-existent departure")
    void testDeleteDepartureByTrainNumberWithNonExistentTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 621, "Holmlia", 4, 13);
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDispatchRegister.deleteDepartureByTrainNumber(622));
    }

    @Test
    @DisplayName("Test if the method throws exception for a departure with invalid trainnumber")
    void testDeleteDepartureByTrainNumberWithInvalidTrainNumber() {
      trainDispatchRegister.addDeparture(12, 13, "L2", 621, "Holmlia", 4, 13);
      assertThrows(
          IllegalArgumentException.class,
          () -> trainDispatchRegister.deleteDepartureByTrainNumber(-2));
    }
  }

  @Nested
  @DisplayName("Display Departure Tests")
  class DisplayDepartureTests {
    @Test
    @DisplayName("Test if the method displays departures within the specified time range")
    void testDisplayDepartureIntervalWithValidInterval() {
      trainDispatchRegister.addDeparture(12, 0, "L1", 601, "Oslo", 2, 5);
      trainDispatchRegister.addDeparture(12, 30, "L2", 602, "Trondheim", 1, 0);

      LocalTime startTime = LocalTime.of(12, 0);
      LocalTime endTime = LocalTime.of(13, 0);

      assertDoesNotThrow(() -> trainDispatchRegister.displayDepartureInterval(startTime, endTime));
    }

    @Test
    @DisplayName("Test if the method throws exception for an invalid time range")
    void testDisplayDepartureIntervalWithInvalidInterval() {
      trainDispatchRegister.addDeparture(12, 0, "L1", 603, "Oslo", 2, 5);
      trainDispatchRegister.addDeparture(12, 30, "L2", 604, "Trondheim", 1, 0);

      LocalTime startTime = LocalTime.of(13, 0);
      LocalTime endTime = LocalTime.of(12, 0);

      assertThrows(
          IllegalArgumentException.class,
          () -> trainDispatchRegister.displayDepartureInterval(startTime, endTime));
    }
  }

  @Nested
  @DisplayName("Test if the getString and ToString methods work as intended")
  class GetStringAndToStringTests {
    @Test
    @DisplayName("Test if the toString method returns the correct string")
    void testGetStringWithNoDelayOrTrack() {
      TrainDeparture departure = new TrainDeparture(12, 34, "L1", 123, "Oslo",  0);
      String result = trainDispatchRegister.getString(departure, "colorCode");
      String expected =
          "| 12:34              | colorCodeL1      \u001B[0m | 123              | Oslo                                       |           | \u001B[31mn/a\u001B[0m       |\n";
      assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test getString method with delay and track")
    void testGetStringWithDelayAndTrack() {
      TrainDeparture departure = new TrainDeparture(12, 34, "L1", 123, "Oslo", 1, 0);
      departure.setDelay(5);
      departure.setTrack(2);
      String result = trainDispatchRegister.getString(departure, "colorCode");
      String expected =
          "| 12:34              | colorCodeL1      \u001B[0m | 123              | Oslo                                       | \u001B[33m5        \u001B[0m | 2         |\n";
      assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test toString method without colors")
    void testToStringWithoutColors() {
      trainDispatchRegister.addDeparture(13,43,"L3",1234,"Oslo",1,0);

      String result = trainDispatchRegister.toString(false);
      String expected =
              "Current time: " + trainDispatchRegister.getClockString() + "\n" +
              "-------------------------------------------------------------------------------------------------------------------------"  + "\n" +
              "|   Departure Time   |   Line   |   Train Number   |                 Destination                |   Delay   |   Track   |" + "\n" +
              "-------------------------------------------------------------------------------------------------------------------------" + "\n" +
              "| 13:43              | L3      [0m | 1234             | Oslo                                       |           | 1         |" + "\n" +
              "-------------------------------------------------------------------------------------------------------------------------";
      assertEquals(expected, result);
    }

  }
}
